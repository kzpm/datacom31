= https://kbase.nl/datacom/31/[N31-41 CCNA1 Introduction to networking (ITN)]
:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud




https://kbase.nl/datacom/31/CiscoPacketTracer_801_Windows_64bit_setup.exe[Download Packet Tracer voor Windows]

== Systeemeisen

- Computer: Microsoft Windows 8.1, 10 (64bit), Ubuntu 20.04 LTS (64bit) or macOS 10.14 or newer.
- amd64(x86-64) CPU
- 4GB of free RAM
- 1.4 GB of free disk space

== Netacad account aanmaken

Wanneer je Packet Tracer gaat gebruiken, moet je bij de start van het programma steeds je _credentials_ (Je account gegevens) invullen.

In het filmpje hieronder zie je hoe je een account in Netacad kunt maken.

https://www.youtube.com/watch?v=TRRQZub6idw[Netacad account maken]

Vergeet niet om de bevestigingsmail te openen en op *Activate* te klikken.
Daarna is je account actief en kunje met Packet Tracer werken.


== Netwerken hebben effect op ons leven

Voor 1990 was internet wereldwijd nog niet zo vanzelfsprekend. 
Wanneer men iets wilde uitwisselen over een grotere afstand, moest het telefonisch of met een fax gebeuren.

Nu zijn netwerken overal.
Berichten, streamen, videoconferencing, cloud, SaaS, virtualisatie alles gaat tegenwoordig via het www.

Politiek gezien is het bestaan van oude geografische grenzen minder belangrijk geworden. Hoewel veel landen met technologie invloeden van buiten proberen tegen te houden. 
De informatievooriening is in de meeste gevallen onmiddellijk. 
Vorming van communities. Denk aan de open-source community!


=== 1.2.1 Host rollen

Alle computers op een netwerk aangesloten en aan netwerkverkeer deelnemen, worden *hosts* genoemd.
Alle hosts hebben een IP adres.

*Servers* zijn in zekere zin ook een host, maar leveren speciale diensten. Bijvoorbeeld web- en mailservers.
Zo'n server verleent diensten aan meerdere *clients*

Een client is in staat om mmeerdere soorten software tegelijk te draaien. Vb. Webpagina bekijken, mailchecken, Berichtenapp lezen, spel spelen.

.Bekende soorten servers
|===
|Web|Hosten van webpagina
|Mail|Hosten van email voor diverse accounts
|File|Gebruikersbestanden opslaan
|===

=== 1.2.2 Peer to Peer
Wanneer meerdere computers direct met elkaar verbonden zijn, dus zonder tussenkomst van een router of switch, speken we van een peer-to-peer network.

.Voor en tegen peer-to-peer
|===
|Voordeel|Nadeel
|Eenvoudig in te stellen|Geen centraal beheer
|Weinig complex|NIet erg veilig
|Goedkoop(Geen aanschaf van dure intermediaire apparaten|Niet schaalbaar
|Goed voor eenvoudige taken (delen van bestanden -NAS- en printers)|Lage prestaties door C/S model
|===

=== 1.2.3 Eindapparaten

Eindapparaten zijn de apparaten die we dagelijks gebruiken voor het opvragen van info. Dus een PC, Laptop, Smartpon, tablet.
Een eindapparaat is altijd de bron of de bestemming van netwerkverkeer.

=== 1.2.4 Intermediaire apparaten
Apparaten die eindapparaten met elkaar verbinden. Dus routers, firewalls en switches.

De apparaten houden een routekaartje bij. Hierbij is de bron en bestemming bepalend voor de route die het verkeer moet afleggen.

Belangrijke taken:

|===
|Regenereren en versturen van communicatiesignalen
|Houden route informatie bij
|Brengen andere apparaten op de hoogt van communicatie/netwerkfouten
|Sturen data langs alternatieve paden wanneer een verbinding wegvalt
|Toestemming of blokkering van bepaalde datastromen (White-blacklisten)
|===

=== 1.2.5 Netwerkmedia

|===
|Koper/metaal kabels|Codering door elektrische pulsen
|Glas of kunstvezel|Codering door lichtpulsen
|Draadloos|modulatie van elektromagnetische golven
|===

Criteria voor het kiezen van een medium:

1. Maximaal lengte voor zend-succes
2. Fysieke omgeving waarin het netwerkmedium moet opereren
3. Hoeveelheid en snelheid waarmee data verzonden moeten worden
4. Kosten


== 1.3 Netwerkweergaven en topologieen

Schematische weergave van de opbouw van een netwerk.

image::http://netwerk800.be/basis/content/2-basisbegrippen/10-topologie/tree-topology.png[]

Hoe zijn netwerken met elkaar verbinden?
1. NIC -> Verbindt eindapparaat met fysiek met netwerk
2. Fysieke poort -> Connector of uitgang van een netwerkapparaat
3. Interface -> Afzonderlijke poorten op een netwwerkapparaat, die vebinding met een bepaald netwerk maken. Ook wel netwerkinterfaces genoemd.

=== 1.3.2 Topologiediagrammen

Verplichten documentatie voor ieder, die met een netwerk werkt.

*Fysiek topologiediagram*
Waar staat de netwerkapparatuur? Hoe zijn de kabelsverbonden? 

image::https://2.bp.blogspot.com/-cMVWMdYQKEA/V-GK5jqPN3I/AAAAAAAAAB8/BhCExH6cBLEw6zvkILdMe-eJpyaJERDZACLcB/s1600/16.PNG[]

*Logisch topologiediagram*
Fysieke locatie ontbreekt. Aangevuld met adresschema's

image::https://www.conceptdraw.com/How-To-Guide/picture/Network-diagram-ISG-topology-diagram-Cisco.png[]

== 1.4 Veelvoorkomende netwerken

=== 1.4.1 Verschillende grootte

Van eenvoudige netwerken: twee netwerken tot zeer complexe netwerken met miljoenen computers.

|===
|Thuisnetwerk|Computers met elkaar verbonden, resources gedeeld. Internet.
|SOHO|Small Home Office|Een lokatie. Toegang tot centraal gedeelde resources, printers, mail, web enz.
|(Middel)grote netwerken|Samenwerking tussen medewerkers. Meerdere locaties. Dusiznden computers mogelijk
|Internet|Het netwerk van netwerken.
|===

=== 1.4.2 Netwerkinfrastructuren LAN's en WAN's

Afhankelijk van:
|===
|Grootte van het gebied
|Aantal aangesloten gebruikers
|Aantal en soorten beschikbare services(Web, Internetbankieren, Mail, opsporing)
|Aard van de verantwoordelijkheden (Bank, Streaming dienst zoals Netflix)
|===



image::http://www.ap-comp.com/images/WAN.jpg[title=Voorbeeld van een WAN en een LAN]

- LAN biedt gebruikers in een beperkt geografisch gebied toegang. Mestal gebruikers van een afdeling, thuisnetwerken, klein bedrijfsnetwerk
- WAN biedt toegang tot andere netwerken over een breed geografisch gebied. Meestal ibv Telecombedrijven.


=== 1.4.3 Intranet en Extranet

Intranet alleen bereikbaar voor werknemers van een specifiek bedrijf.
Extranet biedt beveiligde toegang aan niet werknemers, die toch gebruik moeten kunnen maken van diensten van dat bedrijf.



== 1.5 Internetverbindingen

=== 1.5.1 Internet toegangstechnologieen

Hoe moet je eindgebruikers en organisaties met het internet verbinden?
Meestal via een ISP (Internet Service Provider)

== 1.5.2 SOHO internetverbindingen

Connectie mogelijkheden:

image::http://cdn.onlinewebfonts.com/svg/img_460620.png[title=Satelliet,50,50]

image::https://cdn.iconscout.com/icon/premium/png-512-thumb/dsl-1-209339.png[title=DSL,50,50]

image::https://media.istockphoto.com/vectors/cable-icon-flat-graphic-design-vector-id487561844?k=6&m=487561844&s=612x612&w=0&h=JfT968jVCX4iQmnlosVlXFWmD6WaWlBaN17bdNw9FIk=[title=Kabel (COAX),50,50]

image::https://toppng.com/public/uploads/preview/wireless-icon-transparent-wifi-icon-11553428493jmhabmy8ev.png[title=Draadloos,50,50]

image::https://accelerator.uk.com/wp-content/uploads/2019/01/voip-icon.png[title=Inbelnetwerk niet meer van deze tijd, 50,50]
