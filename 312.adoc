= https://kbase.nl/datacom/31/[N31-41 CCNA1 Introduction to networking (ITN)]
:toc: left
:icons: font
:toclevels: 3
:toc-title: Inhoud




== 1 Doelen van deze module

|===
|Onderwerp|Doel
|Toegang tot het Cisco IOS|Je kunt uitleggen hoe je toegang tot een Cisco IOS apparaat krijgt voor config doeleinden
|IOS-navigatie|Je kunt uitleggen hoe je door het IOS navigeert in verband met configuratie acties
|De commando structuur|Je kunt de commandostructuur van IOS beschrijven en je kent de mogelijkheden van elke mode
|Basisapparaatconfiguratie|Het configureren van een IOS apparaat met behulp van de CLI
|Opslaan van de configuratie|Gebruik de IOS-commando's om de running-conf op te slaan
|Poorten en adressen|Leg uit hoe apparaten over een netwerkmedium communiceren
|Configureren van IP-adressering|Configureer een host met een ip-adres
|Controleer de verbinding|Controleer de verbinding tussen twee eindapparaten
|===


=== 2.1 Toegang tot het Cisco IOS

Er is een besturingssysteem nodig op eindapparaten om https://nl.wikipedia.org/wiki/Kernel[*kernel*] te configureren.
Communicatie met de kernel gebeurt via de *shell*. De gebruiker doet dat via de CLI (Comand Line Interface) of met een GUI (Graphical User Interface).

image::https://upload.wikimedia.org/wikipedia/commons/e/e6/GNOME_Terminal_3.12.png[Een command line interface: de terminal in Linux]

De shell wordt door de gebruiker ingezet om te communiceren met de *kernel*. De kernel communiceert vervolgens met de *hardware*.

==== 2.1.2 GUI

De CLI is een betrouwbare interface om met de kernel te 'praten'. Een GUI heeft vaak vooringestelde opties en heeft lang zoveel mogelijkheden niet als de CLI. Daarom verdient het de voorkeur om met een CLI de kernel te bedienen.
Bij thuisrouters wordt het besturingssysteem meestal *firmware* genoemd.

==== 2.1.3 Doel van een OS

==== 2.1.4 Toegangsmethoden

Een switch dient om dataverkeer door te sturen. Het hoeft niet geconfigureerd worden om te kunnen werken. Dit neemt niet weg dat een switch wel beveiligd moet worden.

Een switch/router kan op drie manieren bereikt worden:

|===
|Console|Een fysieke beheerpoort, die *out-of-band* (= buiten het netwerk om), alleen voor beheerdoeleinden word gebruikt.
|Secure Shell (SSH)| Een *in-band* methode (= Wel over een netwerk bereikbaar) via een SVI (Switch Virtual Interface). Moet met een actieve switchport interface en ip adres geconfigureerd worden.
|Telnet|Telnet is een onveilige in-band methode om op afstand beheeroegang te krijgen. Login gegevens worden in *plain* tekst verstuurd over het netwerk en zijn daardoor zichbaar voor kwaadwillenden. 
|===

image::https://www.infosecmatter.com/wp-content/uploads/2020/01/wireshark-23-telnet.jpg[title=Login gegevens duidelijk zichtbaar met Telnet,400x400] 

==== 2.1.5 Terminal emulatie programma's

Voor het besturingssysteem Windows zijn enkele terminal emulatie programma's verkrijgbaar. 
Meest gebruikt is https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.76-installer.msi[Putty. Downloadlink voor Windows systemen]

Andere programma's voor WIndows zijn Tera Term en SecureCRT.

Wanneer je met Linux werkt, heb je deze programma's niet nodig. SSH is standaard ingebouwd. Ook blinkt Linux uit in CLI. Denk er maar eens over om Windows te vervangen door Linux op je laptop. Als toekomstig IT-er een hele goed beslissing!

==== 2.1.6 Test je kennis: Toegang tot switch/router

https://b.socrative.com/login/student/[Test je kennis]

=== 2.2 IOS navigatie

==== 2.2.1 Belangrijke commando modes

Zie tabel

==== 2.2.2 Configuratie mode en sub-modes

Hieronder een overzicht van de meest voorkomende modes:

|===
|modus|Functie|Herkenbaar aan
|user-EXEC mode| Dit is de mode waarop je binnenkomt in de switch/router. Als het goed is, is deze mode  beveiligd met een wachtwoord. Je kunt hier praktisch niks doen. Je moet wel door naar:|`Switch>`
|privileged-EXEC mode| In deze mode heb je toegang tot alle volgende modes in IOS. De mode is vooral bedoeld om overzichten en statistieken in beeld te krijgen. Daarom is ook deze mode vaak beveiligd met een wachtwoord.|`Switch#`
|global config mode| Deze mode geeft toegang tot globale instellingen voor de switch/router. Zoals een bepalen van een *hostname* of een default gateway.|`Switch(config)#`
|config sub modes|De meest voorkomende zijn de *line-config* mode en de *interface-config* mode|`Switch(config-line)#` en `Switch(config-if)#`
|===  

==== 2.2.3 Video in Netacad

==== 2.2.4 Navigeren tussen de modes in IOS

Wanneer je de CLI op een switch of router start, zul je eerst in de user-EXEC mode komen. Met het commando `enable` of afgekort `en` kom je in de privileged-EXEC mode. De global-config mode bereik je door `configure terminal` of afgekort `conf t` uit te voeren.
Op dat moment kun je verschillende sub-modes in gaan. Met `int f0/1` ga je in dit geval direct een switchport interface configureren. Met het commando `line vty 0` ga je in dit geval iets doen met invoer vanuit de CLI. 

Om een niveau terug te gaan, tik je `exit` in. Wanneer je bijvoorbeeld in de `Switch(config-if)#` mode zit, kom je door `exit` uit te voeren terug in de global-config mode `Switch(config)#`.

Met het commando `end` of de toetsencombi `CTRL+Z`, ga je vanuit elk niveau terug naar de privileged-EXEC mode.

TIP: Gebruik de TAB toets na het intikken van 1, 2 of 3 letters van het commando. Wanneer je ingave correct is, vult het IOS automatisch het juiste commando in. Een handige hulp voor wanneer je onzeker over de spelling van een woord bent!

TIP: Wanneer je achter een woord in een commando wilt weten welke opties er zijn gebruik je het vraagteken (`?`). Je krijgt dan een lijstje te zien met mogelijkheden.

   
==== 2.2.5 Video in Netacad

==== 2.2.6 Syntax Checker

==== 2.2.8 Test je kennis: Navigeren binnen IOS

https://b.socrative.com/login/student/[Test je kennis: Navigeren binnen IOS]

=== 2.3 Commandostructuur
==== 2.3.1 Basis IOS commando structuur

Elk commando heeft een bepaald formaat en syntax. Ook moet elk commando in de juiste mode woren uitgevoerd.

image::https://i.pinimg.com/originals/17/19/a5/1719a57d85ca5c36d9fe815122613ce0.jpg[]

De algemene syntax voor een commando is het commando gevolgd door relevante sleutel woorden (*keywords*) en argumenten of *parameters*

*Voorbeeld:*

*Sleutelwoord*
Een commando of deel van een commando wat door het besturingssysteem herkend zal worden. Bijvoorbeeld `ip address`

*Argument*
Hier gaat het om een waarde of variable, die door de gebruiker ingevoerd wordt. Bijv. _ip address_ `192.168.100.1`

==== 2.3.2 IOS commando synax checker

|===
|Conventie|Beschrijving
|*Vet*|Vetgedrukte tekst zijn sleutelwoorden, die je rechtstreeks op de CLI in kunt voeren. Vb. `ip address`
|_Cursief_|Cursieve tekst geeft argumenten aan, die je zelf dient in te voeren
|[x]|Rechtehaken (Brackets)Geven een optioneel element aan. (Sleutelwoord of argument) Vb. show ip interface _brief_`
|{x}|Accolades (Curly braces)Geven een verplicht element aan, kan sleutelwoord of argument zijn. Vb `show ip _interface fa0/1_`
|[x{y/z}]|Accolades en verticale lijnen binnen rechte haken geven een verplichte keuze binnen een element aan. Vb.`access-group 10 in/out `
|===

==== 2.3.3 Help functies in IOS

|===
|TAB toets| Vult commando's automatisch aan, wanneer er geen fout is getypt
|? Vraagteken achter keyword| Laat de opties zien voor een sleutelwoord of argument
|===

Er zijn ook een groot aantal toetscombinaties mogelijk:

|===
|TAB|Vult een gedeeltelijk commando aan
|Backspace| Wist het karakter links van de cursor
|CTRL+D|Wist het karakter onder de cursor
|CTRL+K|Wist alle karakters tot het einde van de regel
|ESC+D|Wist alle karakters tot het eind van het woord
|CTRL+U of CTRL+X|Wist alle karakters tot het begin van de regel
|CRL+W|Wist het woord links van de cursor
|CTRL+A|Verplaatst de cursor naar het begin van de regel
|Pijl links of CTRL+B|Verplaats de cursor een positie naar links
|ESC+B|Verplaatst de cursor een woord naar links
|ESC+F|Verplaatst de cursor een woord naar rechts
|CTRL+F of pijl naar rechts|Verplaatst de cursor een plaats naar rechts
|CTRL+E|Verplaatst de cursor naar het einde van de regel
|Pijl omhoog of CTRL+P|Haalt commando's uit de historiebuffer. Beginnend bij de meest recente
|CRL+R, CTRL+I of CTRL+L|Toont de systeemprompt na een statusmelding
|===

NOTE: IOS herkent de DEL toets niet

==== Opdracht 2.3.7 Packet Tracer Navigeren in IOS
  
(Zie ELO)

=== 2.4 Basisapparaatconfiguratie

==== 2.4.1 Apparaatnamen

Regels in IOS voor apparaatnamen:

1. Begin met een letter
2. Gebruik *GEEN* spaties
3. Eindig met een letter of cijfer
4. Gebruik alleen letters cijfers en streepjes
5. Gebruik minder dan 64 karakters

Met het commando `hostname` in de globale configuratiemodus, kun je de naam van het apparaat aanpassen.

==== 2.4.2 Regels voor wachtwoorden

1. Gebruik wachtwoorden langer dan 8 tekens
2. Gebruik bij voorkeur een zin die je makkelijk kunt onthouden en wissel hoofd en kleine letters af. Vb. "De kat krabt de krullen van de TRAp"

==== 2.4.3 Wachtwoorden configureren

Wanneer je de toegang tot de user-EXEC mode wil beveiligen, gebruik je de *password* methode. Je kunt daarvoor de volgende opdrachten gebruiken:

[source,bash]
S1# conf t
S1(config)# line console 0
S1(config)# password cisco
S1(config)# login
S1(config)# end
S1#


Met `conf t` bereik je de privileged-EXEC mode. Je bent nu baas over de hele switch!
Met `line console 0` geef je aan dat er maar 1 cnosole gebruikt kan worden om in te loggen. Er mogen dus niet meer sessies geopend worden.
Met het keyword `login` geef je aan dat je de toegang tot de user-EXEC mode wil activeren.


Mocht je de toegang tot de privileged-EXEC mode willen beveiligen (een must voor elke netwerkbeheerder..!) dan gebruik je de *enable secret* methode. 

[source,bash]
S1# conf t
S1(config)# enable secret class
S1(config)# exit
S1#

Wanneer je de toegang ot je switch via SSH of Telnet (liever niet) wil beveiligen, gebruik je weer de *password* methode.
Je moet nu de Virtual Terminal Type invoerregels beveiligen, want via die VTY komen de SSH en Telnet verbindingen binnen. 
Er zijn in principe 16 VTY's tegelijkertijd bruikbaar. (0 t/m 15)

[source,bash]
S1# conf t
S1(config)# line vty 0 15
S1(config)# password cisco
S1(config)# login
S1(config)# end
S1#

==== 2.4.4 Encrypt de wachtwoorden

Open Packet Tracer en stel een wachtwoord in voor het beveiligen van de toegang tot de user-EXEC mode.
Kijk in de privileged-EXEC mode of je je wachtwoord ook kunt zien:

`S1# sh run | se Password` 

[source,bash]
S1# conf t
S1(config)# service password encryption
S1(config)#exit
S1#

Kun je je wachtwoord nu nog teruglezen zoals je hem hebt ingevoerd?


==== 2.4.5 Banner berichten

Het maken van een banner is juridisch gezien een goed middel om gevolgen te geven aan ongeoorloofde toegang.

In de *global-config* mode ku je een banner instellen:


[source,bash]
S1# conf t
S1(config)# banner motd # Alleen geauthoriseerde toegang toegestaan #
S1(config)#exit
S1#

Probeer nu terug te gaan naar de user-EXEC mode en opnieuw `enable` uit te voeren om toegang tot de privileged-EXEC mode te krijgen.
Zie je je net ingevoerde banner staan?

== 2.5 Configuratie opslaan
=== 2.5.1 Configuratiebestanden

Je bent nu op de hoogte van de basisconfiguratie. Je kunt wachtwoorden, en banner berichten aanmaken.
We gaan nu bezig met het opslaan van configuraties.

Er zijn wee systeembestanden die de apparaatconfiguratie opslaan:

1. *start-up config*
Dit is het in NVRAM opgeslagen configuratiebestand. Het bevat alle commando's die het apparaat tijdens het (her)starten gaat gebruiken. Flash verliest de inhoud van zijn geheugen niet bij het uitschakelen van het apparaat.

2. *running-config*
Dit wordt in het RAM opgeslagen. Het geeft de huidige configuratie weer. Het wijzigen van de huidige configuratie heeft direct invloed op het apparaat. Wel verliest het apparaat de inhoud van de wijzigingen wanneer je reboot.

In de privileged-EXEC mode kun je het commando `show running-config` uitvoeren, om de inhoud te bekijken van de huidige configuratie.
Om de startup-config te bekijken gebruik je het commando `show startup-config`.

Wanneer de spanning van het apparaat wegvalt of wanneer het apparaat opnieuw opgestart wordt, gaan alle wijzigingen, die je hebt aangebracht verloren. 
Als je de wijzigingen wilt bewaren, gebruik je het commando `copy running-config startup-config` (Afgekort `copy run start`).
Met dit commando kopieer je de wijzigingen, die je hebt gemaakt naar de startup-configuratie.



==== 2.5.2 De running-config wijzigen

Wanneer je de aangebrachte wijzigen in de running config nog niet hebt opgeslagen, kun je proberen alle ingevoerde wijzigen stuk voor stuk opsproen in de `history` en verwijderen.
Mocht dat teveel werk zijn, dan kun je het apparaat ook opnieuw starten en in de priviliged-EXEC mode het `reload` commando uitvoeren.
 
Een nadeel van het `reload` commando is, dat bij het herladen het apparaat een paar seconden offline is. Dit kan voor downtime in het netwerk zorgen. 

Het `reload` commando is zal vragen of je de wijzigingen, die je eerder in de running-config maakte wil opslaan. In dit geval wil je de wijzigingen negeren, dus tik je `no` of `n` in.

Het kan zijn dat je een aantal ongewenste wijzigingen toch hebt opgeslagen. Dit houdt in dat je de huidige startup-configuratie moet wissen. Ook zal het apparaat opnieuw moeten worden gestart.
Wanneer je de startup-config wilt wissen gebruik je in de privileged-EXEC mode het commando `erase startup-config`. De switch zal nu om een bevestiging vragen. Druk op ENTER om deze te accepteren.
Na het verwijderen van de startup-config uit het NVRAM, wordt het apparaat opnieuw opgestart. In feite vindt er dus een `reload` plaats.
Het apparaat laadt nu de default startup-configuratie, die oorspronkelijk in het apparaat was opgeslagen.

==== 2.5.3 Video (Kun je alleen in Netacad bekijken)

==== 2.5.4 Leg de configuratie vast in een tekstbestand

Configuratiebestanden kunnen ook in een tekstbestand worden opgeslagen. De onderstaande opeenvolgende stappen zorgen voor een werkende kopie, die je ook weer aan kunt passen of voor hergebruik kunt inzetten.

*Linux*
In Linux maak je met `ssh <user>@<ip adres>` verbinding met de switch. 
Wanneer je in de switch bent kun je de running-config opslaan met het commando `copy run scp`. 
Vervolgens geef je het ip adres van je eigen systeem op, de gebruikersnaam van je eigen systeem en de zelfverkozen bestandsnaam.

*Windows*
- Open met Putty of in Linux met ssh een verbinding met je switch of router
Het is wel noodzakelijk dat je het ip-adres of de domain name van je apparaat kent.

image::https://www.cisco.com/c/dam/en/us/support/docs/security-vpn/secure-shell-ssh/4145-ssh-3.gif[]

Je moet dan in Putty logging aan zetten:

image::https://i1.wp.com/www.viktorious.nl/wp-content/uploads/2013/01/2013-01-14_110903.png?w=464&ssl=1[]

- Wanneer je in de console bent, geef je met het commando `show run` of `show start` de inhoud weer. 
Deze wordt in het door jou opgegeven bestand geplaatst. 
Volgens de afbeelding is dat: *&H&Y&M&D.log*. 
De &H&Y&M&D string haalt de huidige tijd op en slaat het op in &H= uur, &Y = huidig jaar, &M = huidige maand, &D = huidige dag.

- Schakel in Putty de logging weer uit:

image::https://kbase.nl/datacom/31/disablelogging.png[title=Logging uitschakelen in Putty, 300,300]

Nadat je het bestand binnen hebt gehaald, kun je het verder bewerken, als documentatie gebruiken of toepassen op een ander apparaat, wat deze lfde configuratie nodig heeft.

*Terugzetten/uploaden van een configuratie*

Om een opgeslagen bestand op je PC naar een switch of router te uploaden, doe je het volgende:

- Kopieer de inhoud van het tekstbestand en plak het in de CLI van de switch of router. 
Elke tekstregel zal nu als commando ingelezen worden. Deze methode is het meest gebruikelijk.


=== 2.5.5 Opdracht Packet Tracer: Configureer de initiele switch instellingen

De instructies staan in het pka bestand. Zet een vinkje bij "Dock" om de schermen netjes naast elkaar te krijgen.

https://kbase.nl/datacom/31/2.5.5-packet-tracer---configure-initial-switch-settings.pka[Download Packet Tracer Activity (pka)]


== 2.6 Poorten en adressen

=== 2.6.1 IP-adressen

Top! Je hebt nu een basisconfiguratie voor het apparaat uitgevoerd!

Er volgt nog meer plezier. Je wilt atuurlijk dat apparaten met elkaar kunnen 'praten'. 
Daarvoor moet je er voor zorgen dat de apparaten correct aangesloten zijn en een *ip-adres* krijgen.

Het gebruik van IP-adressen is een belangrijk middel om apparaten in staat te stellen elkaar te lokaliseren en end-to-end communicatie over het internet tot stand te brengen. Elk eindapparaat in een netwerk moet met een ip-adres geconfigureerd worden.

Voorbeelden van deze eind apparaten zijn:

- Computers (werkstations, laptops, fileservers, webservers)
- Netwerkprinters
- VoiP telefoon
- Beveiligingscamera's
- Smartphones en tablets
- Mobiele handheld apparaten, zoals een draadloze barcode scanner

De notatie voor een ip-adres wordt *dotted-decimal* genoemd en wordt door vier decimale getallen van 0 - 255 weergegeven.
IP4 adressen in een zelfde netwerk (subnet) moeten altijd uniek zijn. Wanneer meerdere apparaten hetzelfde ip-adres hebben, ontstaan er adres conflicten. 
Het ip adres is opgedeeld in een netwerk en een host gedeelte.
In combinatie met het ipv4 adres bepaalt het subnetmasker tot welk subnet het apparaat behoort.

image::https://www.home-network-help.com/images/alternate-ip-address-in-windows-10.jpg[title=De ip configuratie in Windows]

Het voorbeeld in Figuur 3 laat zien dat het ip-adres (192.168.1.25), het subnetmasker (255.255.255.0) en de default gateway (192.168.1.1) aan een host toegewezen zijn.
Het default gateway adres is het ip adres van de router dat de host zal gebruiken voor toegang tot externe netwerken, inclusief het internet.

*IPv6 adressen*

IPv6-adressen zijn 128 bits lang en worden weergegeven als een reeks hexadecimale getallen. Elke vier bits (nibble) staan voor een hezadecimaal cijfer met een totaal aan 32 hexadecimale cijfers. Groepen van 4 hexadecimale cijfers worden door een dubbele punt (:) gescheiden. Ipv6 adressen zijn niet hoofdlettergevoelig en mogen in hoofd- en kleine letters geschreven worden.

image::https://eguibarit.eu/wp-content/uploads/2017/10/Conf-IPv6-2.png[title=Een statisch ipv6 adres configureren, 300,300]

==== 2.6.2 Interfaces en poorten

Netwerkcommunicatie is afhankelijk van interfaces van eindapparaten, interfaces van netwerkapparaten en van de kabels die ze met elkaar verbinden. Elke fysieke interface heeft specificaties, of standaards, die deze definieren. Een kabel die met de interface is verbonden. moet ontworpen zijn om aan de fysieke standaarden van die interface te voldoen. Soorten netwerkmedia zijn twisted-pair koperen kabels, glasvezelkabels, coaxiaalkabels of draadloos.

image::https://www.mytechlogy.com/upload/by_users/ritaliu/291808015128usageoftopofrackswitch.png[title=Switches in een rack,300,500]

image::https://i.ytimg.com/vi/8VhxVZtSRAo/maxresdefault.jpg[title=Een aantal kabeltypen,300,300]

image::https://i1.wp.com/networkustad.com/wp-content/uploads/2019/11/WNICs.png[title=Wireless NIC set, 300,300]

*Verschillen netwerkmedia*

- De afstand waarover het medium succesvol een signaal kan transporteren
- De omgeving waarin het medium kan worden geinstalleerd
- De hoeveelheid data en de snelheid waarmee de data kunnen worde verzonden
- De kosten van het medium en de installatie ervan


Elke link gebruikt een speciale netwerktechnologie.
De meest algemene *Local Area Network* gebruikte technologie is op dit moment *Ethernet*. Ethernetpoorten zijn te vinden op eindapparatuur, switch apparaten en andere apparaten die met een ethernet-kabel fysiek op het netwerk worden aangesloten.

=== 2.7 IP-adressering configureren

==== 2.7.1 Ip adressen handmatig configureren

Elk eindapparaat in een netwerk heeft een ip adres nodig. In deze paragraaf leer je hoe je ip adressen op switches en PC's moet configureren.

Ip adressen kunnen op twee manieren geconfigureerd worden:

1. Handmatig 
2. Automatisch met behulp van het *Dynamic Host Configuration Protocol* (DHCP)

Om een ipv4 adres handmatig te configureren open je in Windows het _Control Panel | Networking Center | Change adapter settings_
en kies je de juiste adapter. Klik vervolgens met de rechtermuisknop en selecteer _Properties_ om de LAN-eigenschappen weer te geven, zoals hieronder te zien:

image::http://i.stack.imgur.com/uX6yX.png[title=Ethernet adapter instellingen]


Klik na selectie van het ipv4 proocol op de knop _Properties_.
Je krijg dan een soortgelijk scherm in beeld:

image::https://websistent.com/wp-content/uploads/2013/07/dns-server-ip-settings.png[title=Handmatig een ip adres invoeren]

NOTE: Het DNS server adres is het ip adres van het *Domain Name System* (DNS)-servers, die gebruikt worden om webadressen (zoals www.cisco.com) om te zetten naar een ip adres.

Een beetje vergelijkbaar met het adresboekje in je telefoon.


In Linux kun je het IP adres van je machine aanpassen in het bestand `/etc/network/interfaces`

image::https://kirelos.com/wp-content/uploads/2020/01/echo/4-29.png[title=Meer configuratie opties in Linux. Bij 'address' vul je het adres van je machine in.]

==== 2.7.2 Automatische IP adresconfiguratie voor eindapparaten

Standaard zijn de meeste eindapparaten ingericht om automatisch een ip adres te verkrijgen van een DHCP server.
Zo'n DHCP server is erg populair, niemand hoeft zelf nog handmatig een ip adres in zijn eindapparaat te configureren.
Dat scheelt bergen werk, zeker voor een netwerkbeheerder, die misschien duizenden eindapparaten onder beheer heeft.

In de onderstaande afbeelding zie je hoe je in Windows DHCP inschakelt:

image::https://i.stack.imgur.com/S3Cnv.png[title=Dynamische toewijzing van IP adressen]

==== 2.7.3 Syntax checker (Via netacad)

==== 2.7.4 Switch Virtual interface configuratie

Om een switch op afstand te kunnen benaderen moet er een ip adres en een subnetmasker ingesteld worden. 
Hiervoor moet je een SVI aanmaken in de switch.
Het is namelijk niet mogelijk om direct een ip-adres op een fysieke switchport te zetten.

Je doet dit als volgt:

 1. Klik op de switch en selecteer het CLI tabblad
 2. Switch> `en`
 3. Switch# `conf t`
 4. Switch(config)# `interface vlan 1`
 5. Switch(config-if)# `ip address 192.168.1.20 255.255.255.0`
 6. Switch(config-if)# `no shut`

==== 2.7.6 Packet Tracer: implementeer basisconnectiviteit

https://kbase.nl/datacom/31/2.7.6-packet-tracer---implement-basic-connectivity.pka[Download Packet Tracer activity 2.7.6]

=== 2.8 Verifieer de connectiviteit

==== 2.8.1 Video activity (Alleen via Netacad omgeving)
==== 2.8.2 Video activity (Alleen via Netacad omgeving)

=== 2.9 Oefeningen en quiz
==== 2.9.1 Packet Tracer - Basis switch- en eindapparaat configuratie

https://kbase.nl/datacom/31/2.9.1-packet-tracer---basic-switch-and-end-device-configuration.pka[Opdracht Packet Tracer Basis-switch- en eindapparaat configuratie PT en instructies]


==== 2.9.2 Lab - Basis switch- en eindapparaat configuratie
Je voert uit:

1. Het opbouwen van een netwerktopologie
2. De PC-hosts configureren
3. Het configureren en controleren van de basisinstellngen op een switch
4. Download de opdracht in pdf formaat

https://kbase.nl/datacom/31/2.9.2-packet-tracer---basic-switch-and-end-device-configuration---physical-mode.pka[2.9.2 Download Lab pka bestand]

https://kbase.nl/datacom/31/2.9.2-packet-tracer---basic-switch-and-end-device-configuration---physical-mode.pdf[2.9.2 Download Lab pdf bestand]

